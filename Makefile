IMAGE=registry.gitlab.com/effiscience/opensource/ftb-kubernetes:latest

build-docker:
	cd docker && docker build --progress=plain -t $(IMAGE) .

push-docker:
	docker push $(IMAGE)

local-install:
	microk8s helm upgrade ftb ./helm --create-namespace --namespace ftb --install
