#!/bin/bash

set -e

if [ ! -f ./eula.txt ]; then
  modpack_installer "$MODPACK_ID" "$MODPACK_VERSION"
  chmod +x ./start.sh
  echo "eula=true" > ./eula.txt
fi

./start.sh
