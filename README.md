# FTB Kuberntes

Install a feed the beast server on a kubernetes server.

## Notes

World data are isolated between two modpack.  
You can change the modpack without loosing data using the helm chart values.

## On ubuntu (and other distro that handle snapcraft)

- Install microk8s

```shell
snap install microk8s --classic
```

- Configure the file `helm/values.yaml`. It's install direworlf 1.20 by default. Look for modpack id/version herre: https://www.feed-the-beast.com/modpacks/server-files
- Install/Update the helm chart

```
microk8s helm upgrade ftb ./helm --create-namespace --namespace ftb --install
```

- Enjoy !
